/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package libinstitute;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
import java.util.*;
//library class contains all the book object
public class Library {
    private String name;
	private ArrayList<Book> books = new ArrayList<Book>();
	Library()
	{

	}
	Library(String name)
	{
		this.name = name;
	}
	public void addBook(Book b)
	{
		books.add(b);
	}
	public Book getBook(String title)
	{
		for(Book b : books)
		{
			if(b.getTitle().equals(title))
			{
				return b;
			}
		}
		return null;
	}
	//print the library books
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for(Book b : books)
		{
			sb.append(b.getTitle()).append(" written by ").append(b.getAuthor()).append("\n");
		}
		return sb.toString();
	}
}
