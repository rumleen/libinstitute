/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package libinstitute;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Book {
    private String title;
	private String author;
	Book()
	{

	}
	Book(String title, String author)
	{
		this.title = title;
		this.author = author;

	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	//author of the book is set with this method
	public void setA(String a)
	{
		//author variable passed is initialize to private variable
		this.author = a;
	}
	public String getTitle()
	{
		return this.title;
	}
	public String getAuthor()
	{
		return this.author;
	}
}
