/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package libinstitute;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class LibInstitute {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //library is created
		Library lb= new Library("New XYZ");
		//book is added.
		Book a= new Book("xyz","abc");
		lb.addBook(a);
		
		System.out.println("Library Book List:");
		System.out.println(lb);
    }
    
}
